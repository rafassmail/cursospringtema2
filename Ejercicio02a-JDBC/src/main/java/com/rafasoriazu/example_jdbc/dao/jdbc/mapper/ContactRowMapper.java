/* 
 * Copyright (c) Curso Introducción Spring Famework2015 
 * Code: https://bitbucket.org/rafassmail/cursospring
 */
package com.rafasoriazu.example_jdbc.dao.jdbc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.rafasoriazu.example_jdbc.model.Contact;

/**
 * 
 * @author Rafa Soriazu (rafasoriazu@gmail.com)
 *
 */
public class ContactRowMapper implements RowMapper<Contact> {
	
	public Contact mapRow(ResultSet rs, int rowNum) throws SQLException {
		 Contact contact = new Contact();
		 contact.setId(rs.getLong(1));
		 contact.setEmail(rs.getString(2));
		 contact.setName(rs.getString(3));
		 return contact;
	}
	

}
