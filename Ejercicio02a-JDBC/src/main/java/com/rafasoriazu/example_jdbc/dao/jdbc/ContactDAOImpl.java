/* 
 * Copyright (c) Curso Introducción Spring Famework2015 
 * Code: https://bitbucket.org/rafassmail/cursospring
 */
package com.rafasoriazu.example_jdbc.dao.jdbc;

import java.util.List;

import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;

import com.rafasoriazu.example_jdbc.dao.ContactDAO;
import com.rafasoriazu.example_jdbc.dao.jdbc.mapper.ContactRowMapper;
import com.rafasoriazu.example_jdbc.model.Contact;

/**
 * 
 * @author Rafa Soriazu (rafasoriazu@gmail.com)
 *
 */
public class ContactDAOImpl implements ContactDAO {
	
	private DataSource dataSource;
	
	public void setDataSource(DataSource ds) {
	    dataSource = ds;
	  }

	/* (non-Javadoc)
	 * @see com.rafasoriazu.example_jdbc.dao.ContactDAO#createContact(com.rafasoriazu.example_jdbc.model.Contact)
	 */
	public void createContact(Contact contact) {
		JdbcTemplate insert = new JdbcTemplate(dataSource);
	    insert.update("INSERT INTO contact (email, name) VALUES(?,?)",
	    new Object[] { contact.getEmail(), contact.getName() });
		
	}

	/* (non-Javadoc)
	 * @see com.rafasoriazu.example_jdbc.dao.ContactDAO#getContacts()
	 */
	public List<Contact> getContacts() {
		 JdbcTemplate select = new JdbcTemplate(dataSource);
		    return select.query("select * from contact",
		        new ContactRowMapper());
	}

	/* (non-Javadoc)
	 * @see com.rafasoriazu.example_jdbc.dao.ContactDAO#getContactsByEmail(java.lang.String)
	 */
	public List<Contact> getContactsByEmail(String email) {
		JdbcTemplate select = new JdbcTemplate(dataSource);
	    return select
	        .query("select * from contact where email = ?",
	            new Object[] { email },
	            new ContactRowMapper());
	}

	/* (non-Javadoc)
	 * @see com.rafasoriazu.example_jdbc.dao.ContactDAO#getContact(java.lang.Long)
	 */
	public Contact getContact(Long id) {
		JdbcTemplate select = new JdbcTemplate(dataSource);
	    return select
	        .query("select * from contact where id = ?",
	            new Object[] { id },
	            new ContactRowMapper()).get(0);
	}

	/* (non-Javadoc)
	 * @see com.rafasoriazu.example_jdbc.dao.ContactDAO#updateContact(com.rafasoriazu.example_jdbc.model.Contact)
	 */
	public void updateContact(Contact contact) {
		JdbcTemplate update = new JdbcTemplate(dataSource);
	    update.update("UPDATE contact SET email = ?, name=? WHERE id=?;",
	        new Object[] { contact.getEmail(), contact.getName(), contact.getId() });
	}

	/* (non-Javadoc)
	 * @see com.rafasoriazu.example_jdbc.dao.ContactDAO#deleteContact(java.lang.Long)
	 */
	public void deleteContact(Long id) {
		JdbcTemplate delete = new JdbcTemplate(dataSource);
	    delete.update("DELETE from contact where id= ? ",
	        new Object[] { id });
		
	}
}
