
/* 
 * Copyright (c) Curso Introducción Spring Famework2015 
  * Code: https://bitbucket.org/rafassmail/cursospring
 */
package com.rafasoriazu.ejercicio02JPA.service.impl;

import static org.springframework.util.Assert.notNull;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rafasoriazu.ejercicio02JPA.model.Contact;
import com.rafasoriazu.ejercicio02JPA.service.ContactService;


/**
 * Contact service bean.
 * 
 * @author: Rafael Soriazu (rafasoriazu@gmail.com)
 */
@Service
@Transactional
public class ContactServiceImpl implements ContactService {
	
	@PersistenceContext
	protected EntityManager entityManager;

		
	@Override
	public void createContact(Contact contact) {
		notNull(contact, "contact can't be null");
		entityManager.persist(contact);		
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Contact> getContacts() {
		return entityManager
			.createQuery("from Contact")
			.getResultList();
	}
	
	

	@Override
	public Contact getContact(Long id) {
		notNull(id, "id can't be null");
		return (Contact) entityManager.find(Contact.class, id);
	}

	@Override
	public void updateContact(Contact contact) {
		notNull(contact, "contact can't be null");
		entityManager.merge(contact);
	}

	@Override
	public void deleteContact(Long id) {
		notNull(id, "id can't be null");
		entityManager.remove(getContact(id));
	}
	
	
	@Override
	public List<Contact> getContactsByEmail(String email){
		notNull(email, "email can't be null");
		return entityManager
				.createNamedQuery("findContactByEmail")
				.setParameter("email", "%" + email + "%")
				.getResultList();
	}

	

	
}
