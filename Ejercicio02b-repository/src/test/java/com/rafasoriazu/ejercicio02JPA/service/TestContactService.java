/* 
 * Copyright (c) Curso Introducción Spring Famework2015 
 * Code: https://bitbucket.org/rafassmail/cursospring
 */

package com.rafasoriazu.ejercicio02JPA.service;


import java.util.List;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.rafasoriazu.ejercicio02JPA.model.Contact;

/**
 * 
 * @author Rafa Soriazu (rafasoriazu@gmail.com)
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:Spring-Main.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestContactService {

	private static final String NAME="Pedro";
	private static final String UPDATED_NAME="Luis";
	private static final String EMAIL="pedro@gmail.com";
	
	
	@Autowired 
	ContactService contactService;
	
	@Test
	public void test01CreateContact() {
		Contact p= new Contact();
		p.setEmail(EMAIL);
		p.setName(NAME);
		contactService.createContact(p);
	}

	@Test
	public void test02GetContacts() {
		List<Contact> list= contactService.getContacts();
		Assert.assertTrue(1==list.size());
	}

	@Test
	public void test03GetContact() {
		List<Contact> list= contactService.getContacts();
		Contact p=contactService.getContact(list.get(0).getId());
		Assert.assertNotNull(p);
		Assert.assertTrue(p.getEmail().equalsIgnoreCase(EMAIL));
	}

	@Test
	public void test04UpdateContact() {
		List<Contact> list= contactService.getContacts();
		Contact p=contactService.getContact(list.get(0).getId());
		p.setName(UPDATED_NAME);
		contactService.updateContact(p);
		list= contactService.getContacts();
		p=contactService.getContact(list.get(0).getId());
		Assert.assertTrue(p.getName().equalsIgnoreCase(UPDATED_NAME));
	}

	@Test
	public void test05DeleteContact() {
		contactService.deleteContact(new Long(1));
		List<Contact> list= contactService.getContacts();
		Assert.assertTrue(list.isEmpty());
	}

}
