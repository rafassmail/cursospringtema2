
/* 
 * Copyright (c) Curso Introducción Spring Famework2015 
  * Code: https://bitbucket.org/rafassmail/cursospring
 */
package com.rafasoriazu.ejercicio02JPA.service.impl;

import static org.springframework.util.Assert.notNull;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rafasoriazu.ejercicio02JPA.dao.ContactDao;
import com.rafasoriazu.ejercicio02JPA.model.Contact;
import com.rafasoriazu.ejercicio02JPA.service.ContactService;


/**
 * Contact service bean.
 * 
 * @author: Rafael Soriazu (rafasoriazu@gmail.com)
 */
@Service
public class ContactServiceImpl implements ContactService {
	
    @Autowired 
    private ContactDao contactDao;
	
		
	@Override
	public void createContact(Contact contact) {
		notNull(contact, "contact can't be null");
		contactDao.save(contact);		
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Contact> getContacts() {
		Iterable<Contact> it=contactDao.findAll();
		if (it==null)return null;
		List<Contact>list= new ArrayList();
		for (Contact c:it){
			list.add(c);
		}
		return list;	
	}
	
	

	@Override
	public Contact getContact(Long id) {
		notNull(id, "id can't be null");
		return contactDao.findOne(id);
	}

	@Override
	public void updateContact(Contact contact) {
		notNull(contact, "contact can't be null");
		contactDao.save(contact);
	}

	@Override
	public void deleteContact(Long id) {
		notNull(id, "id can't be null");
		contactDao.delete(id);
	}
	
	
	@Override
	public List<Contact> getContactsByEmail(String email){
		notNull(email, "email can't be null");
		return contactDao.findByEmailLike(email);
	}

	

	
}
