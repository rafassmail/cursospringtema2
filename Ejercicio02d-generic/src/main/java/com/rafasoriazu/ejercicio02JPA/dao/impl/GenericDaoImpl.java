/* 
 * Copyright (c) Curso Introducción Spring Famework2015 
 * Code: https://bitbucket.org/rafassmail/cursospring
 */
package com.rafasoriazu.ejercicio02JPA.dao.impl;

import java.util.List;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import com.rafasoriazu.ejercicio02JPA.dao.GenericDao;


/**
 * 
 * @author Rafa Soriazu (rafasoriazu@gmail.com)
 *
 */
@Transactional
public abstract class GenericDaoImpl<T> implements GenericDao<T> {

    @PersistenceContext
    protected EntityManager em;

    private Class<T> type;

    public GenericDaoImpl() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class) pt.getActualTypeArguments()[0];
    }

    @Override
    public List<T> list(){
    	return em.createQuery("from "+type.getSimpleName())
    			.getResultList();
    }

    @Override
    public T create(final T t) {
        this.em.persist(t);
        return t;
    }

    @Override
    public void delete(final Object id) {
        this.em.remove(this.em.getReference(type, id));
    }

    @Override
    public T find(final Object id) {
        return (T) this.em.find(type, id);
    }

    @Override
    public T update(final T t) {
        return this.em.merge(t);    
    }
}