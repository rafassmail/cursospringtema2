/* 
 * Copyright (c) Curso Introducción Spring Famework2015 
  * Code: https://bitbucket.org/rafassmail/cursospring
 */

package com.rafasoriazu.ejercicio02JPA.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Contact model class
 * 
 * @author: Rafael Soriazu (rafasoriazu@gmail.com)
 */
@Entity
@Table(name = "contact") 
@NamedQuery(name = "findContactByEmail", query = "from Contact  where email like :email")
public class Contact {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String email;
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
